# GitLab bots

This repo includes scripts to the Gitlab service bots consisting of:

- **Event bot** - reacts to the set-up webhooks
- **Scheduled bot** - is to be scheduled as a (daily) job and periodically perform some actions.

## Common configuration

Create a service account in gitlab with access token. Set the access token as environment variable
`GL_TOKEN` or `GITLAB_TOKEN`.

## Event bot

The event bot is to be called from pipeline as reaction to webhooks and implements following actions:

- New merge request -> set the author as assignee
- New merge request or description change -> if the "manually tested" checkbox in description is checked add the manually tested label
- Comments, suggestion or approve add to the MR -> assign the author as reviewer
- Discussion opened on MR -> requested changes label added
- All discussions resolved -> remove the requested changes label
- MR approved -> resolve all unresolved threads opened by the approver
- "MR included in the release" comment by Service account -> released label added

### Event bot configuration

Following webhooks need to be set up in gitlab:

- Merge request event trigger calling `GL_URL/api/v4/projects/PROJECT_ID/ref/REF_NAME/trigger/pipeline?token=TOKEN&variables[GITLAB_BOT_EVENT]=mr`
- Comment event trigger calling `GL_URL/api/v4/projects/PROJECT_ID/ref/REF_NAME/trigger/pipeline?token=TOKEN&variables[GITLAB_BOT_EVENT]=comment`

Where:

- `GL_URL` is the base url of the gitlab instance, e.g. `https://gitlab.ics.muni.cz`
  - in case access to localhost is not allowed from webhooks (the default), use IP address instead, e.g. `https://147.251.6.102` and disable SSL verification
- `PROJECT_ID` is the id of the project where the pipeline is located, e.g. `5968`
- `REF_NAME` is the branch or the tag name, e.g. `main`
- `TOKEN` is the [pipeline trigger token](https://gitlab.ics.muni.cz/help/ci/triggers/index)

The pipeline then needs to be set up to just run the `gitlab_event_bot` script.

## Scheduled bot

A script meant to be called periodically, will crawl through the configured repositories
and create a summary of merge requests:

- Missing a review - MRs that do not yet have the specified amount of reviewers sorted from the oldest
- (Possibly) ready to be merged - MRs that have at least the specified amount of reviewers, where all the
reviewers have approved the MR and all discussions have been resolved

The formatted summary will be sent as a message to the configured Slack channels.

### Scheduled bot configuration

First a Slack app representing this bot needs to be created with the scopes `app_mentions:read` and
`chat:write`. The minimal Slack app config can then look as follows:

```json
{
  "display_information": {
    "name": "Perun-Gitlab bot"
  },
  "oauth_config": {
    "scopes": {
      "bot": [
        "app_mentions:read",
        "chat:write"
      ]
    }
  },
  "features": {
    "bot_user": {
      "display_name": "Perun-Gitlab bot"
    }
  }
}
```

More information about the Slack app setup to be found [here](https://api.slack.com/tutorials/tracks/getting-a-token).
After creating the app set the Slack access token as environment variable `SLACK_TOKEN`.

Following environment variables need to be set:

- `CHANNEL_FILTERS_DICTIONARY` defines the mappings of type `channel -> filter` where `channel` is the name of the Slack channel to send the notifications about the MRs
  fulfilling the conditions set by the `filter` to. The `filters` are either:
  - List of regular expressions `[regex1, regex2, regex3...]` where each projects on
  path matching at least one of the regexes are satisfied by the filter (meaning its MRs are checked).
  - List of one regular expression starting with `!` like `[!regex]` where only projects
  on path not matching the regex will be included.

You can also override defaults by setting the following environment variables:

- `ROOT_GROUP_IDENTIFIER` the name of the group from which the script will recursively start its
crawl through the projects, defaults to `perun`
- `WAITING_FOR_REVIEW_1_LABEL` the value of the label marking the MR as needing 1 review, defaults to `waiting for review::1`
- `WAITING_FOR_REVIEW_2_LABEL` the value of the label marking the MR as needing 2 reviews, defaults to `waiting for review::2`
- `NEEDED_REVIEWS_DEFAULT` how many reviews are needed by default if no label with this information is present, defaults to 2

Then set up a daily scheduled pipeline and a job in GitLab CI which only runs in scheduled pipelines.
