#!/usr/bin/env python
import os
import re
import gitlab
import json
import sys


GITLAB_BASE_URL = os.environ.get("CI_SERVER_URL")
MANUALLY_TESTED_CHECKBOX_DESC = os.environ.get("MANUALLY_TESTED_CHECKBOX_DESC") or (
    "This MR has been tested or does not change functionality"
)
REQUESTED_CHANGES_LABEL = (
    os.environ.get("REQUESTED_CHANGES_LABEL") or "requested changes"
)
MANUAL_TEST_LABEL = os.environ.get("MANUAL_TEST_LABEL") or "manual test::done"
RELEASED_LABEL = os.environ.get("RELEASED_LABEL") or "released"
WAITING_FOR_REVIEW_1_LABEL = (
    os.environ.get("WAITING_FOR_REVIEW_1_LABEL") or "waiting for review::1"
)
WAITING_FOR_REVIEW_2_LABEL = (
    os.environ.get("WAITING_FOR_REVIEW_2_LABEL") or "waiting for review::2"
)


def on_merge_request_update(merge_request):
    # remove the requested changes label if all threads are resolved and other way around
    try:
        all_resolved = all(
            all(
                not note["resolvable"] or note["resolved"]
                for note in discussion.attributes["notes"]
            )
            for discussion in merge_request.discussions.list(all=True)
        )
        if all_resolved and REQUESTED_CHANGES_LABEL in merge_request.labels:
            print(
                f"Removing label {REQUESTED_CHANGES_LABEL} from MR {merge_request.title}"
            )
            merge_request.labels.remove(REQUESTED_CHANGES_LABEL)
            merge_request.save()
        if not all_resolved and REQUESTED_CHANGES_LABEL not in merge_request.labels:
            print(
                f"Setting label {REQUESTED_CHANGES_LABEL} on MR {merge_request.title}"
            )
            merge_request.labels.append(REQUESTED_CHANGES_LABEL)
            merge_request.save()
    except Exception as e:
        sys.stderr.write(
            f"Checking and (un)setting the requested changes label failed with {e}\n"
        )

    # add the manually tested label if description updated
    try:
        if (
            was_manually_tested(merge_request.description)
            and MANUAL_TEST_LABEL not in merge_request.labels
        ):
            print(f"Setting label {MANUAL_TEST_LABEL} on MR {merge_request.title}")
            merge_request.labels.append(MANUAL_TEST_LABEL)
            merge_request.save()
    except Exception as e:
        sys.stderr.write(f"Setting the manually tested label failed with {e}\n")


def on_comment(merge_request, user_id, comment_type):
    # assign the commenter as a reviewer
    try:
        reviewer_ids = set(map(lambda user: user["id"], merge_request.reviewers))
        if user_id not in reviewer_ids and merge_request.author["id"] != user_id:
            print(f"Setting user {user_id} as reviewer of MR {merge_request.title}")
            merge_request.reviewer_ids = list(reviewer_ids.union({user_id}))
            merge_request.save()
    except Exception as e:
        sys.stderr.write(f"Setting the commenter as reviewer failed with {e}\n")

    # add requested changes label on opened discussion
    if comment_type not in {"DiscussionNote", "DiffNote"}:
        return
    try:
        if REQUESTED_CHANGES_LABEL not in merge_request.labels:
            print(
                f"Setting label {REQUESTED_CHANGES_LABEL} on MR {merge_request.title}"
            )
            merge_request.labels.append(REQUESTED_CHANGES_LABEL)
            merge_request.save()
    except Exception as e:
        sys.stderr.write(f"Setting the requested changes label failed with {e}\n")


def on_release_comment(merge_request):
    try:
        if RELEASED_LABEL not in merge_request.labels:
            print(f"Setting label {RELEASED_LABEL} on MR {merge_request.title}")
            merge_request.labels.append(RELEASED_LABEL)
            merge_request.save()
    except Exception as e:
        sys.stderr.write(f"Setting the released label failed with {e}\n")


def on_merge_request_approval(merge_request, user_id):
    # assign the author of the approval as a reviewer
    try:
        approver_ids = set(
            map(
                lambda approval: approval["user"]["id"],
                merge_request.approvals.get().approved_by,
            )
        )
        reviewer_ids = set(map(lambda user: user["id"], merge_request.reviewers))
        if len(reviewer_ids.union(approver_ids)) != len(reviewer_ids):
            print(
                f"Setting user {list(approver_ids - reviewer_ids)[0]} as reviewer of MR {merge_request.title}"
            )
            merge_request.reviewer_ids = list(approver_ids.union(reviewer_ids))
            merge_request.save()
    except Exception as e:
        sys.stderr.write(f"Setting the approver as reviewer failed with {e}\n")

    # resolve all unresolved threads created by the approver
    try:
        discussions = [
            discussion
            for discussion in merge_request.discussions.list(all=True)
            if discussion.attributes["notes"][0]["author"]["id"] == user_id
            and discussion.attributes["notes"][0]["resolvable"]
            and not discussion.attributes["notes"][0]["resolved"]
        ]
        for discussion in discussions:
            print(f"Resolving all discussions by {user_id} on MR {merge_request.title}")
            discussion.resolved = True
            discussion.save()
    except Exception as e:
        sys.stderr.write(f"Resolving approver's discussions failed with {e}\n")
    return


def on_new_merge_request(merge_request):
    # assign author as assignee
    try:
        if len(merge_request.assignees) == 0:
            print(f"Setting the MR author as an assignee on MR {merge_request.title}")
            author_id = merge_request.author["id"]
            merge_request.assignee_id = author_id
            merge_request.save()
    except Exception as e:
        sys.stderr.write(f"Setting the assignee failed with {e}\n")

    # add labels based on description checkboxes
    try:
        if (
            was_manually_tested(merge_request.description)
            and MANUAL_TEST_LABEL not in merge_request.labels
        ):
            print(f"Setting label {MANUAL_TEST_LABEL} on MR {merge_request.title}")
            merge_request.labels.append(MANUAL_TEST_LABEL)
            merge_request.save()
    except Exception as e:
        sys.stderr.write(f"Setting the manually tested label failed with {e}\n")


def is_service_account(name):
    return (
        False
        if name is None
        else bool(re.compile(r"service", re.IGNORECASE).search(name))
    )


def is_release_comment(comment):
    return (
        False
        if comment is None
        else bool(re.compile(r"semantic-release").search(comment))
    )


def was_manually_tested(description):
    return bool(
        re.compile(r"\[x] " + MANUALLY_TESTED_CHECKBOX_DESC, re.IGNORECASE).search(
            description
        )
    )


# TRIGGER_PAYLOAD is a path to a file with JSON
with open(os.environ.get("TRIGGER_PAYLOAD")) as payload_file:
    payload = json.load(payload_file)
event = os.environ.get("GITLAB_BOT_EVENT")
access_token = (
    os.environ.get("GL_TOKEN")
    if (os.environ.get("GL_TOKEN") is not None)
    else os.environ.get("GITLAB_TOKEN")
)
if payload is None or event is None or access_token is None:
    sys.stderr.write(
        f"Missing required env variables: "
        f" {'TRIGGER_PAYLOAD, ' if payload is None else ''}"
        f" {'GITLAB_BOT_EVENT, ' if event is None else ''}"
        f" {'Gitlab token' if access_token is None else ''}\n"
    )
    exit(1)

if event not in {"comment", "mr"}:
    exit(0)
if is_service_account(payload.get("user", {}).get("name")) and not is_release_comment(
    payload.get("object_attributes", {}).get("note")
):
    exit(0)
if (
    event == "comment"
    and payload.get("object_attributes", {}).get("noteable_type") != "MergeRequest"
):
    exit(0)

project_id = payload.get("project", {}).get("id")
mr_iid = (
    payload.get("object_attributes", {}).get("iid")
    if payload.get("object_attributes", {}).get("iid") is not None
    else payload.get("merge_request", {}).get("iid")
)
if project_id is None or mr_iid is None:
    sys.stderr.write(
        f"Missing required payload items: "
        f" {'Project id, ' if project_id is None else ''}"
        f" {'MR iid' if mr_iid is None else ''}\n"
    )
    exit(1)

mr = None
try:
    print("Fetching the gitlab client")
    gl = gitlab.Gitlab(GITLAB_BASE_URL, private_token=access_token)
    print(f"Getting project {project_id}")
    project = gl.projects.get(project_id)
    print(f"Loading the MRs of project {project_id}")
    mr = project.mergerequests.get(mr_iid)
except Exception as ex:
    sys.stderr.write(f"Fetching the project MR failed with {ex}\n")
    exit(1)

if event == "comment":
    if is_service_account(payload.get("user", {}).get("name")) and is_release_comment(
        payload.get("object_attributes", {}).get("note")
    ):
        on_release_comment(mr)
        exit(0)

    user_id_param = payload.get("user", {}).get("id")
    if user_id_param is None:
        sys.stderr.write("Missing required payload item: User id\n")
        exit(1)
    comment_type_param = payload.get("object_attributes", {}).get("type")
    on_comment(mr, user_id_param, comment_type_param)

if event == "mr":
    mr_action = payload.get("object_attributes", {}).get("action")
    if mr_action is None:
        sys.stderr.write("Missing required payload item: MR action\n")
        exit(1)

    if mr_action == "open":
        on_new_merge_request(mr)
    if mr_action == "approved":
        user_id_param = payload.get("user", {}).get("id")
        if user_id_param is None:
            sys.stderr.write("Missing required payload item: User id\n")
            exit(1)
        on_merge_request_approval(mr, user_id_param)
    if mr_action == "update":
        on_merge_request_update(mr)
