from datetime import datetime, timezone
import gitlab
import os
import sys
import slack_sdk
import re
import json

GITLAB_BASE_URL = os.environ.get("CI_SERVER_URL")
# group to start the crawl at (name or id)
ROOT_GROUP_IDENTIFIER = os.environ.get("ROOT_GROUP_IDENTIFIER") or "perun"
# key is the name of the Slack channels to send the notifications to
# value is a list of filters (regex) to choose to which projects to apply, use ! to negate the regex
CHANNEL_FILTERS_MAP = json.loads(os.environ.get("CHANNEL_FILTERS_DICTIONARY") or "{}")
WAITING_FOR_REVIEW_1_LABEL = (
    os.environ.get("WAITING_FOR_REVIEW_1_LABEL") or "waiting for review::1"
)
WAITING_FOR_REVIEW_2_LABEL = (
    os.environ.get("WAITING_FOR_REVIEW_2_LABEL") or "waiting for review::2"
)
# reviews needed by default
NEEDED_REVIEWS_DEFAULT = os.environ.get("NEEDED_REVIEWS_DEFAULT") or 2

channel_mrs_all_done_map = {channel: [] for channel in CHANNEL_FILTERS_MAP.keys()}
channel_mrs_waiting_for_review_map = {
    channel: [] for channel in CHANNEL_FILTERS_MAP.keys()
}


def is_service_account(name):
    return (
        False
        if name is None
        else bool(re.compile(r"service", re.IGNORECASE).search(name))
    )


def path_match(path, regex):
    return (
        not bool(re.match(regex[1:], path, re.IGNORECASE))
        if len(regex) > 0 and regex[0] == "!"
        else bool(re.match(regex, path, re.IGNORECASE))
    )


def matching_channels(project_path):
    matched_channels = set()
    for channel, regexes in CHANNEL_FILTERS_MAP.items():
        for regex in regexes:
            if path_match(project_path, regex):
                matched_channels.add(channel)
    return matched_channels


def check_mrs(project_id, matched_channels):
    if len(matched_channels) == 0:
        return
    print(f"Getting project {project_id}")
    project = gl.projects.get(project_id, lazy=True)
    print(f"Loading the MRs of project {project_id}")
    mrs = project.mergerequests.list(state="opened", iterator=True)
    for mr in mrs:
        if is_service_account(mr.author.get("name")) or mr.draft or mr.work_in_progress:
            print(
                f"Skipping the MR {mr.title} "
                f"({'Draft' if mr.draft else 'WIP' if mr.work_in_progress else 'Service account author'})"
            )
            continue

        try:
            reviews_needed = NEEDED_REVIEWS_DEFAULT
            if WAITING_FOR_REVIEW_1_LABEL in mr.labels:
                reviews_needed = 1
            if WAITING_FOR_REVIEW_2_LABEL in mr.labels:
                reviews_needed = 2
            approver_ids = set(
                map(
                    lambda approver: approver["user"]["id"],
                    mr.approvals.get().approved_by,
                )
            )
            reviewer_ids = set(map(lambda user: user["id"], mr.reviewers))
            all_resolved = all(
                all(
                    not note["resolvable"] or note["resolved"]
                    for note in discussion.attributes["notes"]
                )
                for discussion in mr.discussions.list(all=True)
            )
            if (
                len(approver_ids) >= reviews_needed
                and reviewer_ids.issubset(approver_ids)
                and all_resolved
            ):
                for channel in matched_channels:
                    channel_mrs_all_done_map[channel].append(mr)

            if len(approver_ids.union(reviewer_ids)) < reviews_needed:
                for channel in matched_channels:
                    channel_mrs_waiting_for_review_map[channel].append(mr)
        except Exception as ex:
            sys.stderr.write(f"Checking the mr {mr.id} failed with {ex}\n")


def construct_slack_message(channel):
    msg = ""
    mrs_waiting_for_review = channel_mrs_waiting_for_review_map[channel]
    mrs_all_done = channel_mrs_all_done_map[channel]

    if len(mrs_waiting_for_review) > 0:
        msg += "Following MRs are waiting for review:\n"
        for mr in mrs_waiting_for_review:
            mr.created_at = datetime.fromisoformat(mr.created_at)
        for mr in sorted(
            mrs_waiting_for_review, key=lambda merge_req: merge_req.created_at
        ):
            days_since_opened = (datetime.now(timezone.utc) - mr.created_at).days
            msg += (
                f"<{mr.web_url}|{mr.title}> - opened {'< ' if days_since_opened < 1 else ''}{days_since_opened}"
                f" day{'s' if days_since_opened != 1 else ''} ago"
                f" with {len(mr.reviewers)} review{'s' if len(mr.reviewers) != 1 else ''} so far\n"
            )
        msg += "\n"

    if len(mrs_all_done) > 0:
        msg += "Following MRs have necessary amount of reviewers and have been approved by them all:\n"
        for mr in mrs_all_done:
            msg += f"<{mr.web_url}|{mr.title}>\n"
    return msg


def crawl_projects(group):
    print(f"Getting group {group.name}")
    group = gl.groups.get(group.id)
    print(f"Loading the projects in group {group.name}")
    projects = group.projects.list(all=True)
    print(f"Loading the subgroups of group {group.name}")
    subgroups = group.subgroups.list(all=True)

    for project in projects:
        if project.archived:
            continue
        matched_channels = matching_channels(project.path_with_namespace)
        check_mrs(project.id, matched_channels)

    for subgroup in subgroups:
        crawl_projects(subgroup)


access_token_gl = (
    os.environ.get("GL_TOKEN")
    if (os.environ.get("GL_TOKEN") is not None)
    else os.environ.get("GITLAB_TOKEN")
)
if access_token_gl is None:
    sys.stderr.write("Required env variable GL_TOKEN or GITLAB_TOKEN was not found.\n")
    exit(1)
access_token_slack = os.environ.get("SLACK_TOKEN")
if access_token_slack is None:
    sys.stderr.write("Required env variable SLACK_TOKEN was not found.\n")
    exit(1)
gl = None
root_group = None
try:
    print("Fetching the gitlab client")
    gl = gitlab.Gitlab(GITLAB_BASE_URL, private_token=access_token_gl)
    print(f"Fetching the root group {ROOT_GROUP_IDENTIFIER}")
    root_group = gl.groups.get(ROOT_GROUP_IDENTIFIER)
except Exception as e:
    sys.stderr.write(f"Fetching the gitlab client and the root group failed with {e}\n")
    exit(1)
slack_client = None
try:
    print("Fetching the slack client")
    slack_client = slack_sdk.WebClient(token=access_token_slack)
except Exception as e:
    sys.stderr.write(f"Fetching the slack client failed with {e}\n")
    exit(1)

crawl_projects(root_group)
# mapping between Slack channel name and the message to send to that channel
channel_msg_map = {
    channel: construct_slack_message(channel) for channel in CHANNEL_FILTERS_MAP.keys()
}
for channel, message in channel_msg_map.items():
    if message == "":
        continue
    try:
        print(f"Sending a slack message to the {channel} channel")
        slack_client.chat_postMessage(channel=channel, text=message)
    except Exception as e:
        sys.stderr.write(
            f"Sending the slack message to channel {channel} failed with {e}\n"
        )
        exit(1)
