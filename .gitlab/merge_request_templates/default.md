## Description

%{first_multiline_commit}

## How to test

TODO: Provide clear steps on how to test this merge request

## Author's checklist

- [ ] I have followed the [contribution guidelines](https://gitlab.ics.muni.cz/perun/perun-idm/perun/-/blob/main/CONTRIBUTING.md?ref_type=heads)
- [ ] This MR has been tested or does not change functionality
- [ ] I have added relevant merge request dependencies (if this MR has any)
- [ ] I have added the correct labels
- [ ] I have assigned reviewers (if any are relevant)
- [ ] I have edited the documentation (if the changes require it) or I have noted the need for the change if I do not have access to the documentation
- [ ] I have marked all introduced BREAKING CHANGES or necessary DEPLOYMENT NOTES in the commit message(s)

## Reviewer's checklist

- [ ] This MR has been tested or does not change functionality
- [ ] This MR has correct commit message format

## Other information 

TODO: Add e.g. example usage, screenshots

## Related issues

TODO: Add relevant issue numbers in the following format (will be added automatically if the MR branch follows our [branch naming conventions](https://gitlab.ics.muni.cz/perun/common/-/blob/main/CONTRIBUTING.md#branch-naming-convention)):

- `closes STR-XXX, closes STR-YYY` for issues fully resolved by this MR, or
- `re STR-XXX, STR-YYY` for issues only partially resolved by this MR
