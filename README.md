# common

Configuration and other files shared between many repositories.

## CI template

This repository includes a template for CI which automatically runs checks, build etc.

The general idea is that no strict checking should be mandatory,
most checks are only run when configuration file for the checking tool is present.

Behavior of some jobs can be modified by CI variables,
and all jobs can be modified by extending.

## Gitlab bots

This repository includes scripts to automate some merge request operations see [bots README](gitlab_bots/README.md).
