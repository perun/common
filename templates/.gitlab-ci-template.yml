### GENERAL ###

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH

default:
  tags:
    - k8s
  interruptible: true
  cache:
    - key:
        files:
          - package-lock.json
      paths:
        - .npm
        - node_modules
      unprotect: true
    - key:
        files:
          - setup.py
          - requirements.txt
      paths:
        - .cache/pip
        - venv
      unprotect: true
    - key:
        files:
          - composer.lock
      paths:
        - .composer-cache
        - vendor
      unprotect: true
    - key:
        files:
          - pom.xml
          - build.gradle
      paths:
        - .m2/repository
        - .gradle
        - build
      unprotect: true

stages:
  - build
  - test
  - release
  - deploy

variables:
  npm_config_cache: "$CI_PROJECT_DIR/.npm"
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"
  COMPOSER_NO_INTERACTION: "1"
  COMPOSER_FUND: "0"
  COMPOSER_CACHE_DIR: "$CI_PROJECT_DIR/.composer-cache"

### TEMPLATES ###

.npm:
  image: ${CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX}/bitnami/node:20
  before_script:
    - npm ci --prefer-offline
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule' && $CI_PIPELINE_SOURCE != 'trigger'"
      exists:
        - "package.json"
        - "package-lock.json"

.pip:
  image: registry.gitlab.ics.muni.cz:443/perun/ci/pipeline-components/python-dev:latest
  before_script:
    - pip install virtualenv
    - virtualenv venv
    - source venv/bin/activate
    - >-
      if [ -f setup.py ]; then
        pip install .${INSTALL_EXTRAS:+[}${INSTALL_EXTRAS}${INSTALL_EXTRAS:+]};
      elif [ -f requirements.txt ]; then
        pip install -r requirements.txt;
      fi
    - >-
      if [ -f requirements-dev.txt ]; then
        pip install -r requirements-dev.txt;
      fi
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule' && $CI_PIPELINE_SOURCE != 'trigger'"
      exists:
        - "setup.py"
        - "requirements.txt"
        - "requirements-dev.txt"

.composer:
  stage: .pre
  image: ${CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX}/composer:2
  script:
    - composer install --ignore-platform-reqs --optimize-autoloader --no-ansi --no-progress --no-scripts
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule' && $CI_PIPELINE_SOURCE != 'trigger'"
      exists:
        - "composer.json"
        - "composer.lock"

### STAGE .pre (caching) ###

npm_ci:
  extends: .npm
  stage: .pre
  script:
    - 'echo "Already done"'

pip_install:
  extends: .pip
  stage: .pre
  script:
    - 'echo "Already done"'

composer_install:
  extends: .composer
  stage: .pre
  script:
    - 'echo "Already done"'

.docker_build_prepare:
  stage: .pre
  image: registry.gitlab.ics.muni.cz:443/perun/ci/pipeline-components/minideb:latest
  script:
    - echo "BUILD_ARGS='NO_BUILD_ARGS=1'" >> docker.env
    - echo "DESTINATIONS='$CI_REGISTRY_IMAGE:latest'" >> docker.env
  artifacts:
    reports:
      dotenv: docker.env
  rules:
    - if: "$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH"
      exists:
        - "Dockerfile"

### STAGE build ###

docker_build:
  stage: build
  image: registry.gitlab.ics.muni.cz:443/perun/ci/pipeline-components/kaniko:latest
  tags:
    - k8s-root
  before_script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"},\"$CI_DEPENDENCY_PROXY_SERVER\":{\"auth\":\"$(printf "%s:%s" ${CI_DEPENDENCY_PROXY_USER} "${CI_DEPENDENCY_PROXY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
  script:
    - >-
      /kaniko/executor
      --context "$CI_PROJECT_DIR$SUBFOLDER"
      --dockerfile "$CI_PROJECT_DIR$SUBFOLDER/Dockerfile"
      $(echo "$DESTINATIONS" | sed 's/\S*/--destination &/g' | xargs)
      $(echo "$BUILD_ARGS" | sed 's/\S*/--build-arg &/g' | xargs)
      --build-arg CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX="$CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX"
      --build-arg CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX="$CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX"
      --build-arg CI_JOB_TOKEN="$CI_JOB_TOKEN"
  variables:
    SUBFOLDER: ""
  rules:
    - if: "$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH"
      exists:
        - "Dockerfile"

# extend docker_build to supply build args and docker tags, one per line:
#   docker_build:
#     variables:
#       MAJOR_VERSION: 10
#       BUILD_ARGS: |-
#         MAJOR_VERSION=$MAJOR_VERSION
#       DESTINATIONS: |-
#         $CI_REGISTRY_IMAGE:$MAJOR_VERSION
# or extend .docker_build_prepare to fill docker.env with BUILD_ARGS and DESTINATIONS:
#   docker_build_prepare:
#     extends: .docker_build_prepare
#     script:
#       - MAJOR_VERSION=10
#       - echo "BUILD_ARGS='MAJOR_VERSION=$MAJOR_VERSION CI=1'" >> docker.env
#       - echo "DESTINATIONS='$CI_REGISTRY_IMAGE:$MAJOR_VERSION'" >> docker.env

maven-build:
  image: registry.gitlab.ics.muni.cz:443/perun/ci/pipeline-components/maven:3-eclipse-temurin-21
  stage: build
  script:
    - mvn clean install
  artifacts:
    paths:
      - "target/*.war"
      - "target/*.jar"
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule' && $CI_PIPELINE_SOURCE != 'trigger'"
      exists:
        - "pom.xml"

gradle-build:
  image: registry.gitlab.ics.muni.cz:443/perun/ci/pipeline-components/gradle:8.10-jdk21
  stage: build
  script:
    - ./gradlew clean build
  artifacts:
    paths:
      - "build/libs/*.jar"
      - "build/libs/*.war"
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule' && $CI_PIPELINE_SOURCE != 'trigger'"
      exists:
        - "build.gradle"

### STAGE test ###

commitlint:
  stage: test
  needs: [] # do not wait
  image: registry.gitlab.ics.muni.cz:443/perun/ci/pipeline-components/commitlint:latest
  script:
    - /app/commitlint.sh
  rules:
    - if: "$CI_MERGE_REQUEST_EVENT_TYPE != 'merge_train' && $CI_MERGE_REQUEST_DIFF_BASE_SHA && $CI_PIPELINE_SOURCE != 'schedule'"
      exists:
        - ".commitlintrc.json"

prettier:
  extends: .npm
  stage: test
  script:
    - npx prettier --check .
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule' && $CI_PIPELINE_SOURCE != 'trigger'"
      exists:
        - ".prettierrc"
        - ".prettierrc.json"

composer_validate:
  stage: test
  image: $CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX/composer:2
  script:
    - composer validate
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule' && $CI_PIPELINE_SOURCE != 'trigger'"
      exists:
        - composer.json

php_compatibility:
  stage: test
  image: registry.gitlab.com/pipeline-components/php-codesniffer:latest
  variables:
    PHPVERSION: "7.4-8.1"
  script:
    - >-
      phpcs -s -p --colors --standard=PHPCompatibility --extensions=php --ignore=vendor,node_modules,venv --runtime-set testVersion ${PHPVERSION} .
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule' && $CI_PIPELINE_SOURCE != 'trigger'"
      exists:
        - "**/*.php"

php_PSR12:
  stage: test
  image: pipelinecomponents/php-codesniffer:latest
  script:
    - phpcs -s -p --colors --extensions=php --standard=PSR12 --ignore=vendor,node_modules,venv .
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule' && $CI_PIPELINE_SOURCE != 'trigger'"
      exists:
        - "**/*.php"

phpunit:
  stage: test
  image: registry.gitlab.com/pipeline-components/phpunit:latest
  script:
    - phpunit --exclude-group node_modules,vendor,venv .
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule' && $CI_PIPELINE_SOURCE != 'trigger'"
      exists:
        - "tests/**/*.php"

.phpunitcomposer:
  stage: test
  image: ${CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX}/php:7.4
  before_script:
    - EXPECTED_CHECKSUM="$(php -r 'copy("https://composer.github.io/installer.sig", "php://stdout");')"
    - php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    - ACTUAL_CHECKSUM="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"
    - >-
      if [ "$EXPECTED_CHECKSUM" != "$ACTUAL_CHECKSUM" ]; then
        >&2 echo 'ERROR: Invalid installer checksum'
        rm composer-setup.php
        exit 1;
      fi
    - php composer-setup.php
    - php composer.phar install
  script:
    - vendor/bin/phpunit --exclude-group node_modules,vendor,venv .
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule' && $CI_PIPELINE_SOURCE != 'trigger'"
      exists:
        - "tests/**/*.php"

# if additional libraries are required for tests, override the default phpunit job like this:
# phpunit:
#   rules:
#     - when: never
# phpunitcomposer:
#   extends: .phpunitcomposer

ruff_format:
  stage: test
  image: registry.gitlab.com/pipeline-components/ruff:latest
  script:
    - ruff format --exclude node_modules --exclude vendor --exclude venv --diff $PYTHON_FILES
  variables:
    PYTHON_FILES: "."
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule' && $CI_PIPELINE_SOURCE != 'trigger'"
      exists:
        - "**/*.py"

ruff_lint:
  stage: test
  image: registry.gitlab.com/pipeline-components/ruff:latest
  script:
    - ruff check --exclude node_modules --exclude vendor --exclude venv --output-format=gitlab $PYTHON_FILES
  variables:
    PYTHON_FILES: "."
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule' && $CI_PIPELINE_SOURCE != 'trigger'"
      exists:
        - "**/*.py"

pytest:
  extends: .pip
  stage: test
  image: registry.gitlab.ics.muni.cz:443/perun/ci/pipeline-components/python-dev:latest
  script:
    - pip install -U pytest
    - >
      pytest -k "not node_modules and not vendor and not venv" .
  rules:
    - if: $CI_PIPELINE_SOURCE != "schedule"
      exists:
        - "pytest.ini"

markdownlint:
  stage: test
  image: registry.gitlab.com/pipeline-components/markdownlint-cli2:latest
  script:
    - >
      markdownlint-cli2 "**/*.md" "#node_modules" "#.npm" "#vendor" "#venv" "#CHANGELOG.md"
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule' && $CI_PIPELINE_SOURCE != 'trigger'"
      exists:
        - ".markdownlint.json"
        - ".markdownlint.yaml"
        - ".markdownlint.yml"

maven_checkstyle:
  image: registry.gitlab.ics.muni.cz:443/perun/ci/pipeline-components/maven:3-eclipse-temurin-21
  stage: test
  script:
    - |
      if [ -f "pom.xml" ] && [ -f "checkstyle.xml" ]; then
        mvn checkstyle:checkstyle
      else
        echo "'pom.xml' is missing. Skipping Maven Checkstyle."
        exit 0
      fi
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule' && $CI_PIPELINE_SOURCE != 'trigger'"
      exists:
        - "checkstyle.xml"


gradle_checkstyle:
  image: registry.gitlab.ics.muni.cz:443/perun/ci/pipeline-components/gradle:8.10-jdk21
  stage: test
  script:
    - |
      if [ -f "build.gradle" ] && [ -f "checkstyle.xml" ]; then
        ./gradlew check
      else
        echo "'build.gradle' is missing. Skipping Gradle Checkstyle."
        exit 0
      fi
  rules:
    - if: "$CI_PIPELINE_SOURCE != 'schedule' && $CI_PIPELINE_SOURCE != 'trigger'"
      exists:
        - "checkstyle.xml"

### STAGE release ###

semantic-release:
  image: registry.gitlab.ics.muni.cz:443/perun/ci/pipeline-components/semantic-release:latest
  stage: deploy
  interruptible: false
  variables:
    NPM_TOKEN: $CI_JOB_TOKEN
  script:
    - /app/semantic-release.sh
  rules:
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH && $RELEASE_FROM_PRODUCTION == null && $CI_PIPELINE_SOURCE != "schedule"'
      exists:
        - ".releaserc"
        - ".releaserc.json"
    - if: '$CI_COMMIT_REF_NAME == "production" && $RELEASE_FROM_PRODUCTION != "" && $CI_PIPELINE_SOURCE != "schedule"'
      exists:
        - ".releaserc"
        - ".releaserc.json"
    - if: '$CI_COMMIT_REF_NAME =~ /^[0-9]+(\.([0-9]+|x))?\.x$/ && $CI_PIPELINE_SOURCE != "schedule"'
      exists:
        - ".releaserc"
        - ".releaserc.json"
