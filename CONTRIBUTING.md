# Contributing to Perun AAI

👋 Welcome to our open-source project!

We're thrilled that you're considering contributing.
Before you dive in, please take a moment to review these guidelines
to ensure a smooth and collaborative experience for everyone involved.

## Branch naming convention

We use the following format for short-lived branch names:

```plain
name/task-id/short_description
```

where:

- **name** is a unique handle of the author ([e-INFRA CZ](https://www.e-infra.cz) login recommended)
- **task-id** is a Jira task ID (optional)
- **short_description** is a short description of the changes using snake_case (in English)

Valid examples:

- `holmes/fix_undefined_variables`
- `cimrman/STR-123/api_method_for_deleting_everything`

## Commit Message Guidelines

### Format

For commit messages, we follow the [conventional commits convention](https://www.conventionalcommits.org/en/v1.0.0/).

The format of the messages should look like this:

```text
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

Ensure that the formatting does not exceed 80 characters per line.

Commit messages should be written in Markdown ([CommonMark](https://commonmark.org/)).

Important notes, including code/configuration examples, are displayed
as markdown in GitHub release details and our UPGRADE.md file.
It is crucial to adhere to this format to avoid losing
any important characters (`*`, `-`, etc.)

### Tooling

To create the message, you can use a tool such as [commitizen](https://www.npmjs.com/package/commitizen).

This tool provides a guide that generates messages in the correct format for you.

If you want to write the body of a commit message on multiple lines,
you can use `\n`.

### Content

Include all changes in the message and especially why you are making them.

Someone will review your work, and the commit message should
clearly convey whether your programming aligns with it.

Avoid brief descriptions like "something was added" or "something was deleted".

Describe any other significant circumstances,
such as what needs to be done before merging the commit into the stable branch
(typically changing configuration files, dependency on another commit or merge request).

For a detailed guide on how commits and their messages should look,
check out [Git Commit Good Practice](https://wiki.openstack.org/wiki/GitCommitMessages).

### Breaking Changes

In semantic versioning, breaking changes can be specified.
These changes result in a major version increase.
For such commits, add `BREAKING CHANGE: <description>` to the commit message footer.
The description must specify what the breaking change involves.

Examples:

* `upgrade DB version from the changelog`
* `define oidc_client_id property in instanceConfig`

The description of the BREAKING CHANGE will appear in the `CHANGELOG.md`
or `UPGRADE.md` file, which contains automatically generated release notes.
Therefore, the BREAKING CHANGE description should be written to be understandable
on its own, without the rest of the commit message.

Generally, these are changes that make the application unsuitable for deployment
(not just a failure but also a change in the application's original behavior)
with any original configuration,
or that cause users to lose a previously accessible feature.

More specific definitions of what is considered are BREAKING CHANGE
are in each repository, where applicable.

### Deployment Notes

Together with BREAKING CHANGES, semantic release is configured to look for
an additional custom footer: `DEPLOYMENT NOTE: <description>`.
This description should include any additional information that
the deployment team needs to know during deployment
(e.g., it is recommended to enable a default-disabled feature).

The description usually contains a list of instructions that
need to be performed during deployment. After the release, the DEPLOYMENT NOTE
will also appear in the `CHANGELOG.md` or `UPGRADE.md` file
for automatically generated release notes. Therefore the text
in the DEPLOYMENT NOTE must be self-explanatory,
without relying on the rest of the commit message.

A single commit can include both footers, i.e., a BREAKING CHANGE
and a DEPLOYMENT NOTE. In such cases, both will be written
into the `CHANGELOG.md` or `UPGRADE.md` file.
